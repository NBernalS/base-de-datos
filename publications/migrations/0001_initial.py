# Generated by Django 3.2.8 on 2021-10-07 05:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Publication',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('date', models.DateTimeField(null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('image', models.ImageField(blank=True, upload_to='images')),
                ('mail', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='my_email', to='users.user')),
            ],
        ),
    ]
