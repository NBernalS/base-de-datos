from rest_framework import views, status
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.parsers import MultiPartParser, FormParser
#model
from publications.models import Publication
#serializer
from publications.serializers import PublicationSerealizer

# List , Create  
class PublicationView(generics.ListCreateAPIView):
    parser_classes = [MultiPartParser, FormParser]
    queryset = Publication.objects.all()
    serializer_class = PublicationSerealizer 

# Read, update, delete
class PublicationRetrieveupdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Publication.objects.all()
    serializer_class = PublicationSerealizer





