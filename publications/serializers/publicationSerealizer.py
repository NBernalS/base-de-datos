from rest_framework import fields, serializers
from publications.models.publication import Publication



class PublicationSerealizer(serializers.ModelSerializer):
    class Meta:
        model = Publication
        fields = [ 'id', 'mail', 'profile_picture', 'date', 'description','image']
        