from django.contrib import admin
from .models.publication import Publication


class PublicationAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('id', 'mail', 'profile_picture', 'date', 'image')
    search_fields = ['id']
    list_filter = ('mail',)

# Register your models here.
admin.site.register(Publication, PublicationAdmin)