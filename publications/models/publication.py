from django.db import models
from users.models.users import User

class Publication(models.Model):
    id = models.AutoField(primary_key=True)
    mail = models.ForeignKey(User, related_name='my_email', on_delete=models.SET_NULL, null=True)
    profile_picture =models.ForeignKey(User, related_name="profile_picture", on_delete=models.SET_NULL, null=True)
    date = models.DateTimeField(null=True)
    description = models.TextField(null=True, blank=True)
    image =  models.ImageField(upload_to='images', blank=True)

    