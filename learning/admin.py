from django.contrib import admin
from .models.learning import Learning

class LearningAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ("publicacion", "autor", "informacion")
    search_fields = ["autor"]
    list_filter = ("autor",)
# Register your models here.
admin.site.register(Learning, LearningAdmin)

