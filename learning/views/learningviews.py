from rest_framework import views, status
from rest_framework.response import Response
from rest_framework import generics
from learning.models import Learning
from learning.serializers import LearningSerializer

class LearningView (generics.ListCreateAPIView):
    queryset=Learning.objects.all()
    serializer_class=LearningSerializer

class LearningRetrieveupdate (generics.RetrieveUpdateDestroyAPIView):
    queryset = Learning.objects.all()
    serializer_class = LearningSerializer
    