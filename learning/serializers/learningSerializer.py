from rest_framework import serializers, fields
from learning.models.learning import Learning

class LearningSerializer(serializers.ModelSerializer):
    class Meta:
        model = Learning
        fields = ["publicacion", "autor", "informacion"]