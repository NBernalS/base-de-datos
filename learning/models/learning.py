from django.db import models 

class Learning (models.Model):
    publicacion = models.AutoField(primary_key=True)
    autor = models.CharField(blank=True, max_length=100)
    informacion = models.TextField(max_length=1000, help_text="Informacion adicional...")
