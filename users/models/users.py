from django.db import models

class User(models.Model):
    email = models.EmailField(primary_key=True, blank=True)
    password = models.CharField(blank=True, max_length=30)
    date_birth = models.DateField(null=True)
    full_name = models.CharField(blank=True, max_length=70)
    picture_profile= models.ImageField(upload_to="picture_profile", blank=True)
    facebook = models.URLField(blank=True, max_length=200)
    twitter = models.URLField(blank=True, max_length=200)
    instagram = models.URLField(blank=True, max_length=200)
    youtube = models.URLField(blank=True, max_length=200)
    

    
