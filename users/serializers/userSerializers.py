from rest_framework import fields, serializers
from users.models.users import User

class UserSerializers(serializers.ModelSerializer):
    class Meta: 
        model = User
        fields = ["email" , "full_name", "picture_profile", "facebook" , "twitter" , "instagram" , "youtube"]
        