from rest_framework import views, status
from rest_framework import generics
from rest_framework.parsers import MultiPartParser, FormParser

#model
from users.models import User
#serializador
from users.serializers import UserSerializers

# List , Create  
class UsersView(generics.ListCreateAPIView):
    parser_classes = [MultiPartParser, FormParser]
    queryset = User.objects.all()
    serializer_class = UserSerializers

# Read, update, delete
class UserRetrieveupdate(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializers
