from django.contrib import admin
from .models.users import User

class UserAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('email', 'date_birth', 'full_name', 'picture_profile')
    search_fields = ['full_name']
    list_filter = ('email',)

# Register your models here.

admin.site.register(User, UserAdmin)