from rest_framework import fields, serializers
from comments.models.comments import Comments

class CommentsSerializers(serializers.ModelSerializer):
    class Meta: 
        model = Comments
        fields = ['id_comments', 'id_publication', 'content', 'users_email', 'picture_profile']
        