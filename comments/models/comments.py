from django.db import models
from publications.models.publication import Publication
from users.models.users import User

class Comments(models.Model):
    id_comments = models.AutoField(primary_key=True)
    id_publication = models.ForeignKey(Publication, related_name="id_publication", on_delete=models.SET_NULL, null=True)
    content = models.TextField(blank=True, null=True)
    users_email = models.ForeignKey(User, related_name="users_email", on_delete=models.SET_NULL, null=True)
    picture_profile = models.ForeignKey(User, related_name="profile", on_delete=models.SET_NULL, null=True)
