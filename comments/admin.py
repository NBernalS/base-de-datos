from django.contrib import admin
from comments.models.comments import Comments

class CommentsAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('id_comments', 'content', 'users_email' ,'picture_profile' )
    search_fields = ['id_comments']
    list_filter = ('users_email',)

# Register your models here.

admin.site.register(Comments, CommentsAdmin)