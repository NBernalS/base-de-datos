from rest_framework import views, status
from rest_framework import generics
from rest_framework.parsers import MultiPartParser, FormParser

#model
from comments.models import Comments
#serializador
from comments.serializers import CommentsSerializers

# List , Create  
class CommentsView(generics.ListCreateAPIView):
    queryset = Comments.objects.all()
    serializer_class = CommentsSerializers

# Read, update, delete
class CommentsRetrieveupdate(generics.RetrieveUpdateDestroyAPIView):
    queryset = Comments.objects.all()
    serializer_class = CommentsSerializers
