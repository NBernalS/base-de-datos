from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from learning.views.learningviews import LearningView , LearningRetrieveupdate
from publications.views import PublicationView,PublicationRetrieveupdateDestroy
from users.views.usersViews import UsersView , UserRetrieveupdate
from comments.views.commentsViews import CommentsView , CommentsRetrieveupdate
from inBox.views.messageView import MessageView , MessageRUD



urlpatterns = [
    path('admin/', admin.site.urls),
    path('learning/', LearningView.as_view()),
    path('learning/<int:pk>', LearningRetrieveupdate.as_view()),
    path('publications/', PublicationView.as_view()),
    path('publication/<int:pk>', PublicationRetrieveupdateDestroy.as_view()),
    path('users/', UsersView.as_view()),
    path('user/<str:pk>', UserRetrieveupdate.as_view()),
    path('comments/', CommentsView.as_view()),
    path('comment/<int:pk>', CommentsRetrieveupdate.as_view()),
    path('messages/',MessageView.as_view()),
    path('messages/<int:pk>',MessageRUD.as_view()),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
