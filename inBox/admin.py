from django.contrib import admin
from .models.message import Message

class MessageAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('id', 'message', 'from_email', 'to_email')
    search_fields = ['message']
    list_filter = ('from_email',)
    
# Register your models here.
admin.site.register(Message,MessageAdmin)