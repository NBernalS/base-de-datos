from rest_framework import fields, serializers
from inBox.models.message import Message

class MessageSerealizer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = [ 'id', 'message', 'from_email', 'to_email','date',]
        