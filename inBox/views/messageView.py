from rest_framework import generics
#model
from inBox.models.message import Message
#serializer
from inBox.serializer.messageSerializer import MessageSerealizer

# List , Create
class MessageView(generics.ListCreateAPIView):
    queryset=Message.objects.all()
    serializer_class=MessageSerealizer

# Read, update, delete
class MessageRUD(generics.RetrieveUpdateDestroyAPIView):
    queryset=Message.objects.all()
    serializer_class=MessageSerealizer
