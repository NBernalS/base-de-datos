from django.db import models
from users.models.users import User
from datetime import datetime

class Message(models.Model):
    id = models.AutoField(primary_key=True)
    to_email = models.ForeignKey(User, related_name='to_email', on_delete=models.SET_NULL, null=True)
    from_email = models.ForeignKey(User, related_name='from_email', on_delete=models.SET_NULL, null=True)
    message = models.TextField(max_length=1000)
    date = models.DateField(default=datetime.now)